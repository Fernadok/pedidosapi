﻿using Moq;
using Newtonsoft.Json;
using NUnit.Framework;
using Pedidos.WebAPI.Configuration;
using Pedidos.WebAPI.ModelView;
using RestSharp;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Pedidos.Tests
{
    public class FileTest
    {
        private readonly HttpClient httpClient = new HttpClient();

        [Test]
        public void GetJsonTest()
        {
            var client = new RestClient("https://jsonplaceholder.typicode.com/photos");
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);

            // Deserializamos el json en un objeto.
            var parseData = JsonConvert.DeserializeObject<List<Photos>>(response.Content);

            Assert.IsTrue(parseData.Count > 0);
        }

    }
}
