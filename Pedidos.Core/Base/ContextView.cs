﻿using PagedList.Core;
using System.Collections.Generic;

namespace Pedidos.Core.Base
{
    public class ContextView<T> where T : class
    {
        public IEnumerable<T> Data { get; set; }
        public IPagedList MetaData { get; set; }
    }
}
