﻿namespace Pedidos.Core.Base
{
    public interface IEntityBase<TPrimaryKey>
    {
        public long Id { get; set; }
        bool IsActive { get; set; }
    }
}
