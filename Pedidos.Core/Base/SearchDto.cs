﻿using System;

namespace Pedidos.Core.Base
{
    /// <summary>
    /// Objeto para el filtrado, orden y paginación.
    /// </summary>
    public class SearchViewDto
    {
        public SearchViewDto()
        {
            Search = string.Empty;
            Page = 1;
            PageSize = 10;
            OrderByColumn = "Id";
            Order = "desc";
        }

        /// <summary>
        /// Identity.
        /// </summary>
        public long Id { get; set; }
      
        /// <summary>
        /// Fieald text.
        /// </summary>
        public string Search { get; set; }

        /// <summary>
        /// Current page.
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// Order column (desc/asc).
        /// </summary>
        public string Order { get; set; }

        /// <summary>
        /// Count item per page.
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// order by column.
        /// </summary>
        public string OrderByColumn { get; set; }

        /// <summary>
        /// Date From.
        /// </summary>
        public DateTime? DateFrom { get; set; }

        /// <summary>
        /// Date To.
        /// </summary>
        public DateTime? DateTo { get; set; }

    }
}
