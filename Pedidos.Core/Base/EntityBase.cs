﻿using System;
using System.Text;

namespace Pedidos.Core.Base
{
    public abstract class EntityBase<TPrimaryKey> : IEntityBase<TPrimaryKey>
    {
        public EntityBase()
        {
            IsActive = true;
        }

        public long Id { get; set; }
        public bool IsActive { get; set; }
    }
}
