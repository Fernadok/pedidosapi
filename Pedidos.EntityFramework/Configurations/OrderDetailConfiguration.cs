﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pedidos.EntityFramework.Domain;

namespace Pedidos.EntityFramework.Configurations
{
    public class OrderDetailConfiguration : IEntityTypeConfiguration<OrderDetail>
    {
        public void Configure(EntityTypeBuilder<OrderDetail> builder)
        {
            builder.HasKey(x => x.Id);

            builder
                .HasOne(p => p.Order)
                .WithMany(c => c.OrderDetails)
                .HasForeignKey(p => p.OrderId)
                .IsRequired();

            builder
                .HasOne(p => p.Product)
                .WithMany(c => c.OrderDetails)
                .HasForeignKey(p => p.ProductId)
                .IsRequired();

            builder
                 .Property(p => p.Total)
                 .HasColumnType("decimal(18,2)");
        }
    }
}
