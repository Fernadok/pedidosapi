﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pedidos.EntityFramework.Domain;

namespace Pedidos.EntityFramework.Configurations
{
    public class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.HasKey(x => x.Id);

            builder
              .HasOne(p => p.Client)
              .WithMany(c => c.Orders)
              .HasForeignKey(p => p.ClientId)
              .IsRequired();

            builder
             .Property(p => p.TotalPrice)
             .HasColumnType("decimal(18,2)");
        }
    }
}
