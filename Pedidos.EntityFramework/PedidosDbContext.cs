﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Pedidos.Core.Base;
using Pedidos.EntityFramework.Configurations;
using Pedidos.EntityFramework.Domain;
using Pedidos.EntityFramework.Migrations.Seed;
using System.Threading.Tasks;

namespace Pedidos.EntityFramework
{
    public class PedidosDbContext : IdentityDbContext
    {
        //Ejecicio A
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Client> Clients { get; set; }

        //Ejecicio B
        public DbSet<Sample> Samples { get; set; }

        public PedidosDbContext(DbContextOptions options) 
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new OrderConfiguration());
            modelBuilder.ApplyConfiguration(new OrderDetailConfiguration());
            modelBuilder.ApplyConfiguration(new ClientConfiguration());
            modelBuilder.ApplyConfiguration(new ProductConfiguration());

            // Seed //
            modelBuilder.ApplyConfiguration(new ClientData());
            modelBuilder.ApplyConfiguration(new ProductData()); 
            modelBuilder.ApplyConfiguration(new OrderData());
            modelBuilder.ApplyConfiguration(new OrderDetailData());
            modelBuilder.ApplyConfiguration(new SampleData());
        }

    }
}
