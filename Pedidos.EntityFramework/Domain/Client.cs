﻿using Pedidos.Core.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Pedidos.EntityFramework.Domain
{
    public class Client : EntityBase<long>
    {
        public Client()
        {
            Orders = new HashSet<Order>();
        }

        [Required]
        public string Fullname { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public string Telephone { get; set; }

        #region Properties Mapping
        public virtual HashSet<Order> Orders { get; set; }   
        #endregion
    }
}
