﻿using Pedidos.Core.Base;
using System.ComponentModel.DataAnnotations;

namespace Pedidos.EntityFramework.Domain
{
    public class OrderDetail : EntityBase<long>
    {
        public OrderDetail()
        {
        }

        [Required]
        public long Quantity { get; set; }

        public decimal Total { get; set; }

     
        #region Properties Mapping  
        public long OrderId { get; set; }
        public virtual Order Order { get; set; }

        public long ProductId { get; set; }
        public virtual Product Product { get; set; }
        #endregion
    }
}
