﻿using Pedidos.Core.Base;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Pedidos.EntityFramework.Domain
{
    public class Product : EntityBase<long>
    {
        public Product()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }

        [Required]
        public string Code { get; set; }

        [Required]
        public string Description { get; set; }

        public decimal Price { get; set; }


        #region Properties Mapping  
        public virtual HashSet<OrderDetail> OrderDetails { get; set; }
        #endregion
    }
}
