﻿using Pedidos.Core.Base;

namespace Pedidos.EntityFramework.Domain
{
    public class Sample : EntityBase<long>
    {
        public string Content { get; set; }
        public long Qty { get; set; }
    }
}
