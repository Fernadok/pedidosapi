﻿using Pedidos.Core.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Pedidos.EntityFramework.Domain
{
    public class Order : EntityBase<long>
    {
        public Order()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }

        [Required]
        public DateTime PurchaseDate { get; set; }

        [Required]
        public decimal TotalPrice { get; set; }

        #region Properties Mapping      
        public long ClientId { get; set; }
        public virtual Client Client { get; set; }
  
        public virtual HashSet<OrderDetail> OrderDetails { get; set; }    
        #endregion
    }
}