﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pedidos.EntityFramework.Domain;
using System;
using System.Collections.Generic;

namespace Pedidos.EntityFramework.Migrations.Seed
{
    public class SampleData : IEntityTypeConfiguration<Sample>
    {
        public void Configure(EntityTypeBuilder<Sample> builder)
        {
            var SampleList = new List<Sample>();
            for (int i = 1; i <= 50000; i++)
            {
                SampleList.Add(new Sample
                {
                    Id = i,
                    Content = "Content " + i,
                    Qty = i
                });
            }

            builder.HasData(SampleList);
        }
    }
}
