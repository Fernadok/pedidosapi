﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pedidos.EntityFramework.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pedidos.EntityFramework.Migrations.Seed
{
    public class ClientData : IEntityTypeConfiguration<Client>
    {
        public void Configure(EntityTypeBuilder<Client> builder)
        {
            builder.HasData(new Client { 
                Id = 1 ,
                Fullname="Fernando E.",
                Address="Sarmiento 930",
                Telephone="026645 1545878"
            }, new Client
            {
                Id = 2,
                Fullname = "Daneil E.",
                Address = "San Juan 5789",
                Telephone = "011 45879656"
            });
        }
    }
}
