﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pedidos.EntityFramework.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pedidos.EntityFramework.Migrations.Seed
{
    public class OrderDetailData : IEntityTypeConfiguration<OrderDetail>
    {
        public void Configure(EntityTypeBuilder<OrderDetail> builder)
        {
            var orderDetailList = new List<OrderDetail>();
            var id = 1;

            for (int i = 1; i <= 50; i++)
            {
                for (int r = 0; r < new Random().Next(1, 7); r++)
                {
                    orderDetailList.Add(new OrderDetail
                    {
                        Id = id,
                        OrderId = i,
                        ProductId = new Random().Next(1, 51),
                        Quantity = i + 5 * 2,
                        Total = i + 20
                    });

                    id++;
                }          
            }

            builder.HasData(orderDetailList);
        }
    }
}
