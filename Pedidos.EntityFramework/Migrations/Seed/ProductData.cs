﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pedidos.EntityFramework.Domain;
using System;
using System.Collections.Generic;

namespace Pedidos.EntityFramework.Migrations.Seed
{
    public class ProductData : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            var prodList = new List<Product>();
            for (int i = 1; i <= 50; i++)
            {
                prodList.Add(new Product { 
                    Id = i,
                    Code = Guid.NewGuid().ToString(),
                    Description = $"Product Desc {i}",
                    Price = i + 5 + 2
                });
            }

            builder.HasData(prodList);
        }
    }
}
