﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pedidos.EntityFramework.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pedidos.EntityFramework.Migrations.Seed
{
    public class OrderData : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            var orderList = new List<Order>();
            for (int i = 1; i <= 50; i++)
            {
                orderList.Add(new Order
                {
                    Id = i,
                    ClientId = new Random().Next(1,3),
                    PurchaseDate = DateTime.UtcNow,
                    TotalPrice=989
                });
            }

            builder.HasData(orderList);
        }
    }
}
