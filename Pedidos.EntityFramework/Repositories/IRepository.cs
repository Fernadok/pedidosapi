﻿using System.Linq;
using System.Threading.Tasks;

namespace Pedidos.EntityFramework.Repositories
{
    public interface IRepository<TEntity> where TEntity : class, new()
    {
        IQueryable<TEntity> GetAll();
      
        Task<TEntity> InsertAsync(TEntity entity);
      
        Task<TEntity> UpdateAsync(TEntity entity);

        Task<bool> Delete(TEntity entity);

        Task<TEntity> GetByIdAsync(long id);
    }
}