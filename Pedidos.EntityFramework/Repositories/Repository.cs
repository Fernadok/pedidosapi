﻿using Microsoft.EntityFrameworkCore;
using Pedidos.Core.Base;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Pedidos.EntityFramework.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : EntityBase<long>, new()
    {
        protected readonly PedidosDbContext PedidosContext;

        public Repository(PedidosDbContext pedidosContext)
        {
            PedidosContext = pedidosContext;
        }

        public IQueryable<TEntity> GetAll()
        {
            try
            {
                return PedidosContext.Set<TEntity>();
            }
            catch (Exception ex)
            {
                throw new Exception($"Couldn't retrieve entities: {ex.Message}");
            }
        }

        public async Task<TEntity> InsertAsync(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(InsertAsync)} entity must not be null");
            }

            try
            {
                await PedidosContext.AddAsync(entity);
                await PedidosContext.SaveChangesAsync();

                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(entity)} could not be saved: {ex.Message}");
            }
        }

        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(UpdateAsync)} entity must not be null");
            }

            try
            {
                PedidosContext.Update(entity);
                await PedidosContext.SaveChangesAsync();

                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(entity)} could not be updated: {ex.Message}");
            }
        }

        public async Task<bool> Delete(TEntity entity)
        {
            try
            {
                entity.IsActive = false;
                await UpdateAsync(entity);
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(entity)} could not be deleted: {ex.Message}");
            }
        }

        public async Task<TEntity> GetByIdAsync(long id)
        {
            try
            {
               return await PedidosContext.Set<TEntity>().SingleOrDefaultAsync(s => s.Id == id);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
