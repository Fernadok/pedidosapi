PedidosAPI (Proyecto evaluación)

Requerimientos:
- ASPNET CORE 5
- SQL SERVER 2012+

Paso para levantar la solución:
- Restaurar paquetes nuget.
- Configurar el connectionString (SQL Server).
- Package Manager Console => setar el proyecto "PedidosAPI.EntityGramework".
- Ejecutar el comando => update-database.
- Presionar F5.
- Listo.
