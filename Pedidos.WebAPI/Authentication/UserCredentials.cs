﻿using System.ComponentModel.DataAnnotations;

namespace Pedidos.WebAPI.Authentication
{
    public class UserCredentials
    {
        [EmailAddress]
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
