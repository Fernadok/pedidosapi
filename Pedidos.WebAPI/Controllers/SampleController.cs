﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Pedidos.Application.Sample;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pedidos.WebAPI.Controllers
{
    [Route("api/samples")]
    [ApiController]
    public class SampleController : ControllerBase
    {
        private CancellationTokenSource _cancellationTokenSource;
        private readonly ISampleService _sampleService;
        private readonly IMapper _mapper;

        public SampleController(ISampleService sampleService, IMapper mapper)
        {
            _cancellationTokenSource = new CancellationTokenSource();
            _sampleService = sampleService;
            _mapper = mapper;
        }

        [HttpGet("runSumQuantityJson")]
        public async Task<RespuestaTarea[]> RunSumQuantity()
        {
            try
            {
                return await _sampleService.RunSumQuantity(_cancellationTokenSource.Token);
            }
            catch (TaskCanceledException ex)
            {
                throw new System.Exception($"Message: {ex.Message}");
            }
            finally
            {
                _cancellationTokenSource?.Dispose();
                _cancellationTokenSource = null;
            }
        }

        [HttpGet("cancellationTasks")]
        public async Task<RespuestaTarea[]> CancellationTasks()
        {
            try
            {
                var respuesta = RunSumQuantity();
                _cancellationTokenSource?.Cancel();
                _cancellationTokenSource.Dispose();
             
                return await respuesta;
            }
            catch (TaskCanceledException ex)
            {
                throw new System.Exception($"Message: {ex.Message}");
            }
        }

        [HttpGet("joinTwoList")]
        public async Task<IEnumerable<RespuestaTarea>> JoinTwoList()
        {
           var listA = await _sampleService.RunSumQuantity(_cancellationTokenSource.Token);
           var listB = await _sampleService.RunSumQuantity(_cancellationTokenSource.Token);

            var query = from a in listA
                        join b in listB
                             on a.Total equals b.Total
                        select _mapper.Map<RespuestaTarea>(a);

            return query;
        }
    }
}
