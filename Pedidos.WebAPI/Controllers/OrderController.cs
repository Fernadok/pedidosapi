﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Pedidos.Application.Order;
using Pedidos.Application.Order.Dto;
using Pedidos.Core.Base;
using System.Net;
using System.Threading.Tasks;

namespace Pedidos.WebAPI.Controllers
{
    [Route("api/orders")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        /// <summary>
        /// Metodo para obtención de ordenes.
        /// </summary>
        /// <param name="search">Objeto para la busqueda de ordenes.</param>
        /// <returns></returns>
        [HttpPost("getAll")]
        public ActionResult<ContextView<OrderDto>> GetAll(SearchViewDto search)
        {
            return _orderService.GetAll(search);
        }

        /// <summary>
        /// Actualiza la cantidad de un producto en una orden.
        /// </summary>
        /// <param name="idItemDetail">Id del detálle de la orden.</param>
        /// <param name="quantity">Cantidad de productos de un item.</param>
        /// <returns></returns>
        [HttpPost("updateProductDetail")]
        public async Task<ActionResult<bool>> UpdateProductDetail(long idItemDetail, int quantity)
        {
            return await _orderService.UpdateProductDetail(idItemDetail, quantity);
        }
    }
}
