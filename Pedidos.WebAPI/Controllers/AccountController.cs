﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Pedidos.EntityFramework;
using Pedidos.WebAPI.Authentication;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Pedidos.WebAPI.Controllers
{
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly UserManager<IdentityUser> userManager;
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly IConfiguration configuration;
        private readonly PedidosDbContext context;

        public AccountController(UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
            IConfiguration configuration,
            PedidosDbContext context)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.configuration = configuration;
            this.context = context;
        }

        [HttpPost("create")]
        public async Task<ActionResult<ResponseAuth>> Create([FromBody] UserCredentials credencials)
        {
            var user = new IdentityUser { UserName = credencials.Email, Email = credencials.Email };
            var result = await userManager.CreateAsync(user, credencials.Password);

            if (result.Succeeded)
            {
                return await BuildToken(credencials);
            }
            else
            {
                return BadRequest(result.Errors);
            }
        }

        [HttpPost("login")]
        public async Task<ActionResult<ResponseAuth>> Login([FromBody] UserCredentials credencials)
        {
            var result = await signInManager.PasswordSignInAsync(credencials.Email, credencials.Password,
                isPersistent: false, lockoutOnFailure: false);

            if (result.Succeeded)
            {
                return await BuildToken(credencials);
            }
            else
            {
                return BadRequest("Login error");
            }
        }

        private async Task<ResponseAuth> BuildToken(UserCredentials credencials)
        {
            var claims = new List<Claim>()
            {
                new Claim("email", credencials.Email)
            };

            var user = await userManager.FindByEmailAsync(credencials.Email);
            var claimsDB = await userManager.GetClaimsAsync(user);

            claims.AddRange(claimsDB);

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["keyjwt"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var expiration = DateTime.UtcNow.AddYears(1);

            var token = new JwtSecurityToken(issuer: null, audience: null, claims: claims,
                expires: expiration, signingCredentials: creds);

            return new ResponseAuth()
            {
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                Expiration = expiration
            };
        }
    }
}
