﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Pedidos.WebAPI.Configuration;
using Pedidos.WebAPI.ModelView;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Pedidos.WebAPI.Controllers
{
    [Route("api/files")]
    [ApiController]
    public class FileController : ControllerBase
    {
        private readonly HttpClient httpClient = new HttpClient();
        private readonly IFileStorage _fileStorage;

        public FileController(IFileStorage fileStorage)
        {
            _fileStorage = fileStorage;
        }

        [HttpGet("getJson")]
        public async Task<ActionResult<string>> Get()
        {
            try
            {
                var client = new RestClient("https://jsonplaceholder.typicode.com/photos");
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);

                // Deserializamos el json en un objeto.
                var parseData = JsonConvert.DeserializeObject<List<Photos>>(response.Content);

                // Guardamos el Json en disco.
                return await _fileStorage.Save("Download", response.Content);
            }
            catch (HttpRequestException e)
            {
                throw new Exception($"Message: {e.Message}");
            }
            catch (Exception e)
            {
                throw new Exception($"Message: {e.Message}");
            }
        }
    }
}
