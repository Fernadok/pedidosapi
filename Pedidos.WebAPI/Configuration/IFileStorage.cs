﻿
using System.Threading.Tasks;

namespace Pedidos.WebAPI.Configuration
{
    public interface IFileStorage
    {
        Task<string> Save(string container, string textJson);
    }
}
