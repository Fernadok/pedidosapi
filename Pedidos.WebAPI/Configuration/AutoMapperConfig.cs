﻿using AutoMapper;
using Pedidos.Application.Client.Dto;
using Pedidos.Application.Order.Dto;
using Pedidos.EntityFramework.Domain;
using System.Collections.Generic;

namespace Pedidos.WebAPI.Configuration
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<Order, OrderDto>()
                .ForMember(x => x.Client, options => options.MapFrom(MapearClient))
                .ForMember(x => x.OrderDetails, options => options.MapFrom(MapearOrderDetail));
        }

        private ClientDto MapearClient(Order order, OrderDto orderDto)
        {
            var resultado = new ClientDto();

            if (order.Client != null)
            {
                resultado.Id = order.ClientId;
                resultado.Fullname = order.Client.Fullname;
                resultado.Telephone = order.Client.Telephone;
            }

            return resultado;
        }

        private List<OrderDetailDto> MapearOrderDetail(Order order, OrderDto orderDto)
        {
            var resultado = new List<OrderDetailDto>();
            if (order.OrderDetails != null)
            {
                foreach (var detail in order.OrderDetails)
                {
                    resultado.Add(new OrderDetailDto
                    {
                        Id = detail.Id,
                        Product = new Application.Product.Dto.ProductDto
                        {
                            Id = detail.Product.Id,
                            Code = detail.Product.Code,
                            Description = detail.Product.Description,
                            Price = detail.Product.Price
                        },
                        Quantity = detail.Quantity,
                        Total= detail.Total
                    });
                }
            }

            return resultado;
        }
    }
}
