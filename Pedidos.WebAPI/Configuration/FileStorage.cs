﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Pedidos.WebAPI.Configuration
{
    public class FileStorage : IFileStorage
    {
        private readonly IWebHostEnvironment env;
        private readonly IHttpContextAccessor httpContextAccessor;
       
        public FileStorage(IWebHostEnvironment env, IHttpContextAccessor httpContextAccessor)
        {
            this.env = env;
            this.httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// Guarda texto en disco.
        /// </summary>
        /// <param name="container">Carpeta donde se va colocar el archivo.</param>
        /// <param name="textJson">Texto que se va almacenar.</param>
        /// <returns>Ruta donde se guardo el archivo.</returns>
        public async Task<string> Save(string container, string textJson)
        {
            var extension = ".json";
            var filename = $"{Guid.NewGuid()}{extension}";
            string folder = Path.Combine(env.WebRootPath, container);

            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }

            string ruta = Path.Combine(folder, filename);
            using (StreamWriter writetext = new StreamWriter(ruta))
            {
               await writetext.WriteLineAsync(textJson);
            }

            var url= $"{httpContextAccessor.HttpContext.Request.Scheme}://{httpContextAccessor.HttpContext.Request.Host}";
            var pathLocal = Path.Combine(url, folder, filename).Replace("\\", "/");
            return pathLocal;
        }
    }
}
