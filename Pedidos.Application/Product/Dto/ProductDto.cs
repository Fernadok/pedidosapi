﻿using Pedidos.Core.Base;

namespace Pedidos.Application.Product.Dto
{
    public class ProductDto : EntityBase<long>
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
    }
}
