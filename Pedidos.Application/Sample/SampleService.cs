﻿using Pedidos.Core;
using Pedidos.EntityFramework.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Pedidos.Application.Sample
{
    public class SampleService : ISampleService
    {
        private readonly IRepository<EntityFramework.Domain.Sample> _sampleRepository;

        public SampleService(
            IRepository<EntityFramework.Domain.Sample> sampleRepository)
        {
            _sampleRepository = sampleRepository;
        }

        public async Task<RespuestaTarea[]> RunSumQuantity(CancellationToken cancellationToken = default)
        {
            var samples = _sampleRepository.GetAll().ToList();
            List<Task<RespuestaTarea>> tareas = new List<Task<RespuestaTarea>>();

            try
            {
                for (int i = 0; i < 10; i++)
                {
                    tareas.Add(GetTotal(samples, cancellationToken));
                }

                var respuestasTareas = Task.WhenAll(tareas);
                return await respuestasTareas;
            }
            catch (Exception ex)
            {
                throw new Exception("operación cancelada: " + ex.Message);
            }
        }

        private async Task<RespuestaTarea> GetTotal(List<EntityFramework.Domain.Sample> samples,
             CancellationToken cancellationToken = default)
        {
            RespuestaTarea respuestaTarea = new RespuestaTarea();

            return await Task.Run(() =>
            {
                // Se comenta para simular un error.
                // samples.Sum(s=>s.Qty);

                string registro = string.Empty;
                long total = 0;

                foreach (var item in samples)
                {
                    try
                    {
                        total += item.Qty + 1;
                        registro = item.Id.ToString();

                        if (item.Id == 100 || item.Id == 500 || item.Id == 1000)
                        {
                            throw new Exception("Error forzado.");
                        }
                    }
                    catch (Exception ex)
                    {
                        respuestaTarea.Estados.Add(new Estado
                        {
                            Exception = ex.Message,
                            RegistroId = registro
                        });
                    }
                }

                respuestaTarea.Total = total;
                return respuestaTarea;
            });
        }

        public async Task<RespuestaTarea[]> CancellationTasks(CancellationToken cancellationToken = default)
        {
            var rspuesta = RunSumQuantity();
            return await rspuesta;
        }
    }

    public class RespuestaTarea
    {
        public RespuestaTarea()
        {
            Estados = new List<Estado>();
        }

        public long Total { get; set; }
        public List<Estado> Estados { get; set; }
    }

    public class Estado
    {
        public string Exception { get; set; }
        public string RegistroId { get; set; }
    }
}
