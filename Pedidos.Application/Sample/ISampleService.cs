﻿using System.Threading;
using System.Threading.Tasks;

namespace Pedidos.Application.Sample
{
    public interface ISampleService
    {
        Task<RespuestaTarea[]> RunSumQuantity(CancellationToken cancellationToken = default);

        Task<RespuestaTarea[]> CancellationTasks(CancellationToken cancellationToken = default);
    }
}