﻿using Pedidos.Core.Base;

namespace Pedidos.Application.Client.Dto
{
    public class ClientDto : EntityBase<long>
    {
        public string Fullname { get; set; }

        public string Address { get; set; }

        public string Telephone { get; set; }
    }
}
