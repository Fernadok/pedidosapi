﻿using Pedidos.Application.Product.Dto;
using Pedidos.Core.Base;

namespace Pedidos.Application.Order.Dto
{
    public class OrderDetailDto : EntityBase<long>
    {
        public long Quantity { get; set; }
        public decimal Total { get; set; }

        public virtual ProductDto Product { get; set; }
    }
}
