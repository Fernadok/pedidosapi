﻿using AutoMapper;
using Pedidos.Application.Client.Dto;
using Pedidos.Core.Base;
using System;
using System.Collections.Generic;

namespace Pedidos.Application.Order.Dto
{
    public class OrderDto : EntityBase<long>
    {
        public OrderDto()
        {
            OrderDetails = new HashSet<OrderDetailDto>();
        }

        public DateTime PurchaseDate { get; set; }
        public decimal TotalPrice { get; set; }
      
        public virtual ClientDto Client { get; set; }

        public virtual HashSet<OrderDetailDto> OrderDetails { get; set; }
    }
}
