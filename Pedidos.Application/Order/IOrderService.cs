﻿using Pedidos.Application.Order.Dto;
using Pedidos.Core.Base;
using System.Threading.Tasks;

namespace Pedidos.Application.Order
{
    public interface IOrderService
    {
        ContextView<OrderDto> GetAll(SearchViewDto search);

        Task<bool> UpdateProductDetail(long id, int quantity);
    }
}
