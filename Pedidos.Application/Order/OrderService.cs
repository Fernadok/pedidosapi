﻿using Microsoft.EntityFrameworkCore;
using PagedList.Core;
using Pedidos.Application.Order.Dto;
using Pedidos.Core;
using Pedidos.Core.Base;
using Pedidos.EntityFramework.Repositories;
using System.Linq;
using AutoMapper;
using System.Linq.Expressions;
using System;
using System.Threading.Tasks;

namespace Pedidos.Application.Order
{
    public class OrderService : IOrderService 
    {
        private readonly IRepository<EntityFramework.Domain.Order> _orderRepository;
        private readonly IRepository<EntityFramework.Domain.OrderDetail> _orderDetailRepository;
        private readonly IMapper _mapper;

        public OrderService(
            IRepository<EntityFramework.Domain.Order> orderRepository,
            IRepository<EntityFramework.Domain.OrderDetail> orderDetailRepository,
            IMapper mapper)
        {
            _orderRepository = orderRepository;
            _orderDetailRepository = orderDetailRepository;
            _mapper = mapper;
        }

        public ContextView<OrderDto> GetAll(SearchViewDto filters)
        {
            var notEmpty = !string.IsNullOrEmpty(filters.Search);

            //** Se agrega algúnos criterios de filtrado.**//
            Expression<Func<EntityFramework.Domain.Order, bool>> expression = 
                x => (notEmpty && (
                        filters.Id > 0 && x.Id == filters.Id
                        || (x.Client.Fullname.ToLower().Contains(filters.Search.ToLower()))
                        || (x.Client.Telephone.ToLower().Contains(filters.Search.ToLower()))
                        || (x.Client.Address.ToLower().Contains(filters.Search.ToLower()))
                        || (x.OrderDetails.Any(a=>a.Total.ToString().Contains(filters.Search)))
                        || (x.OrderDetails.Any(a=>a.Product.Code.Contains(filters.Search))))
                     ) || !notEmpty;

            var resultList = _orderRepository.GetAll()
                            .Include(s => s.Client)
                            .Include(s => s.OrderDetails).ThenInclude(x => x.Product)
                            .Where(x => x.IsActive)
                            .Where(expression)
                            .OrderByTo(filters.OrderByColumn, filters.Order.ToLower() == "desc")
                            .ToPagedList(filters.Page, filters.PageSize);

            return new ContextView<OrderDto>
            {
                Data = resultList.Select(x => _mapper.Map<OrderDto>(x)),
                MetaData = resultList.GetMetaData()
            };
        }

        /// <summary>
        /// Actualiza la cantidad de un producto en una orden.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> UpdateProductDetail(long id, int quantity)
        {
            var detail = await _orderDetailRepository.GetAll()
                .Include(i=>i.Product)
                .Include(i => i.Order)
                .FirstOrDefaultAsync(x=>x.Id == id);
            
            detail.Quantity = quantity;
            detail.Total = detail.Product.Price * quantity;
            detail.Order.TotalPrice = detail.Order.OrderDetails.Sum(s=>s.Total);
          
            await _orderDetailRepository.UpdateAsync(detail);

            return true;
        }
    }
}
